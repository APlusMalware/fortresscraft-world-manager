﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Synergy;
using Synergy.Installation;
using Synergy.Xml;

namespace FortressCraft_Evolved_World_Manager
{
    public class Manager
    {
        private static readonly String registryKey = "HKEY_LOCAL_MACHINE\\SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\Steam App 254200";
        private static readonly String worldsPath = Path.Combine("AppData", "LocalLow", "ProjectorGames", "FortressCraft", "Worlds");
        private static readonly String dataPath = Path.Combine(Environment.Is64BitOperatingSystem ? "64" : "32", "Default", "Data");

        private Dictionary<String, Research> _research;
        private Dictionary<Int32, ItemType> _itemTypes;
        private Dictionary<UInt16, CubeType> _cubeTypes;
        private Dictionary<String, Recipe> _recipes;
        public String InstallDir { get; private set; }

        public String WorldsDir { get; private set; }

        public Manager()
        {
            InstallDir = ReadFortressCraftInstallDir();
            WorldsDir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile), worldsPath);
        }

        private static String ReadFortressCraftInstallDir()
        {
            return (String)Microsoft.Win32.Registry.GetValue(registryKey, "InstallLocation", null);
        }

        public List<World> GetWorlds()
        {
            return World.ReadWorlds(WorldsDir);
        }

        public Dictionary<String, Research> GetResearch()
        {
            if (_research == null)
            {
                XmlData xmlData = new XmlData(Path.Combine(InstallDir, dataPath, "Research.xml"));
                _research = Research.CreateDictionary(xmlData.ReadXmlToType<List<Research>>());
            }
            return _research;
        }

        public Dictionary<Int32, ItemType> GetItemTypes()
        {
            if (_itemTypes == null)
            {
                XmlData xmlData = new XmlData(Path.Combine(InstallDir, dataPath, "Items.xml"));
                _itemTypes = ItemType.CreateDictionary(xmlData.ReadXmlToType<List<ItemType>>());
            }
            return _itemTypes;
        }
        public Dictionary<UInt16, CubeType> GetCubeTypes()
        {
            if (_cubeTypes == null)
            {
                XmlData xmlData = new XmlData(Path.Combine(InstallDir, dataPath, "TerrainData.xml"));
                _cubeTypes = CubeType.CreateDictionary(xmlData.ReadXmlToType<List<CubeType>>());
            }
            return _cubeTypes;
        }

        public Dictionary<String, Recipe> GetRecipes()
        {
            if (_recipes == null)
            {
                XmlData manufacturerXmlData = new XmlData(Path.Combine(InstallDir, dataPath, "ManufacturerRecipes.xml"));
                XmlData refineryXmlData = new XmlData(Path.Combine(InstallDir, dataPath, "RefineryRecipes.xml"));
                XmlData smelterXmlData = new XmlData(Path.Combine(InstallDir, dataPath, "SmelterRecipes.xml"));
                List<Recipe> manufacturerRecipes = manufacturerXmlData.ReadXmlToType<List<Recipe>>();
                List<Recipe> refineryRecipes = refineryXmlData.ReadXmlToType<List<Recipe>>();
                List<Recipe> smelterRecipes = smelterXmlData.ReadXmlToType<List<Recipe>>();
                IEnumerable<Recipe> allRecipes = manufacturerRecipes.Concat(refineryRecipes).Concat(smelterRecipes);
                _recipes = Recipe.CreateDictionary(allRecipes).ToDictionary(k => k.Key, r => r.Value.First());
            }
            return _recipes;
        }
    }
}
