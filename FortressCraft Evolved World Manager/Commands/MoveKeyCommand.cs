﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace FortressCraft_Evolved_World_Manager.Commands
{
    public class MoveKeyCommand : ICommand
    {
        private readonly ObservableCollection<String> _destination;
        private readonly ObservableCollection<String> _source;

        public MoveKeyCommand(ObservableCollection<String> destination, ObservableCollection<String> source)
        {
            _destination = destination;
            _source = source;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public Boolean CanExecute(Object parameter)
        {
            return parameter is String;
        }

        public void Execute(Object parameter)
        {
            String key = parameter as String;
            _destination.Add(key);
            _source.Remove(key);
        }
    }
}
