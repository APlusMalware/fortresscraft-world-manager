﻿using System;
using System.Windows.Input;
using FortressCraft_Evolved_World_Manager.ViewModels;

namespace FortressCraft_Evolved_World_Manager.Commands
{
    public class ShowChooseWorldCommand : ICommand
    {
        private readonly Main _main;
        public ShowChooseWorldCommand(Main main)
        {
            _main = main;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public Boolean CanExecute(Object parameter)
        {
            return true;
        }

        public void Execute(Object parameter)
        {
            _main.ActiveView = Main.View.ChooseWorld;
        }
    }
}
