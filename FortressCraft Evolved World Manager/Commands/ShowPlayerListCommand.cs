﻿using System;
using System.Linq;
using System.Windows.Input;
using FortressCraft_Evolved_World_Manager.ViewModels;

namespace FortressCraft_Evolved_World_Manager.Commands
{
    public class ShowPlayerListCommand : ICommand
    {
        private readonly Main _main;
        public ShowPlayerListCommand(Main main)
        {
            _main = main;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public Boolean CanExecute(Object parameter)
        {
            return parameter is WorldFacade;
        }

        public void Execute(Object parameter)
        {
            WorldFacade world = parameter as WorldFacade;
            _main.ActiveView = Main.View.ChoosePlayer;
            if (_main.PlayerList.World != world)
            {
                var players = Synergy.Player.ReadPlayers(world.BackingWorld).Select(p => new PlayerFacade(p));
                _main.PlayerList = new PlayerList(players, world);
            }
        }
    }
}
