﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using FortressCraft_Evolved_World_Manager.ViewModels;

namespace FortressCraft_Evolved_World_Manager.Commands
{
    public class ShowPlayerResearchCommand : ICommand
    {
        private readonly Main _main;
        public ShowPlayerResearchCommand(Main main)
        {
            _main = main;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public Boolean CanExecute(Object parameter)
        {
            return parameter is PlayerFacade;
        }

        public void Execute(Object parameter)
        {
            PlayerFacade player = parameter as PlayerFacade;

            _main.PlayerResearch = new PlayerResearch(player, _main.GetProjectKeys(), _main.GetRecipeKeys());

            _main.ActiveView = Main.View.PlayerResearch;
        }
    }
}
