﻿using System;
using System.Windows.Input;
using FortressCraft_Evolved_World_Manager.ViewModels;
using Synergy;

namespace FortressCraft_Evolved_World_Manager.Commands
{
    public class SavePlayerInventoryCommand : ICommand
    {
        private readonly Main _main;

        public SavePlayerInventoryCommand(Main main)
        {
            _main = main;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public Boolean CanExecute(Object parameter)
        {
            return parameter is PlayerFacade;
        }

        public void Execute(Object parameter)
        {
            PlayerFacade player = parameter as PlayerFacade;
            if (player != null)
            {
                Item[,] items = new Item[Inventory.InventoryWidth, Inventory.InventoryHeight];
                for (Int32 i = 0; i < Inventory.InventoryHeight; i++)
                {
                    for (Int32 j = 0; j < Inventory.InventoryWidth; j++)
                    {

                        Item item = _main.PlayerInventory.Items[i*Inventory.InventoryWidth + j].BackingItem;
                        items[j, i] = item;
                    }
                }
                player.BackingPlayer.Inventory.Items = items;

                player.BackingPlayer.SaveInventoryFile((Int32)Player.InventoryVersion.One);
            }
        }
    }
}
