﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using FortressCraft_Evolved_World_Manager.ViewModels;
using Synergy;
using Synergy.Installation;

namespace FortressCraft_Evolved_World_Manager.Commands
{
    public class ShowChooseItemCommand : ICommand
    {
        private readonly Main _main;
        public ShowChooseItemCommand(Main main)
        {
            _main = main;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public Boolean CanExecute(Object parameter)
        {
            return parameter is PlayerInventoryItem;
        }

        public void Execute(Object parameter)
        {
            PlayerInventoryItem item = parameter as PlayerInventoryItem;
            Dictionary<Int32, ItemType> itemTypes = _main.GetItemTypes();
            Dictionary<UInt16, CubeType> cubeTypes = _main.GetCubeTypes();
            ChooseItem viewModel = new ChooseItem(itemTypes, cubeTypes.Select(kvp => kvp.Value), item?.BackingItem);
            ChooseItemWindow window = new ChooseItemWindow(viewModel);
            window.ShowDialog();
            Item newItem = viewModel.SelectedItem;
            if (newItem.ItemCategory == ItemCategory.CubeStack)
                item.CubeType = cubeTypes[((ItemCubeStack)newItem).CubeTypeId];
            else
                item.ItemType = itemTypes[newItem.ItemTypeId];
            item.BackingItem = newItem;
        }
    }
}
