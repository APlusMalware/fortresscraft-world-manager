﻿using System;
using System.Windows.Input;
using FortressCraft_Evolved_World_Manager.ViewModels;

namespace FortressCraft_Evolved_World_Manager.Commands
{
    public class SavePlayerSettingsCommand : ICommand
    {
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public Boolean CanExecute(Object parameter)
        {
            return parameter is PlayerFacade;
        }

        public void Execute(Object parameter)
        {
            PlayerFacade player = parameter as PlayerFacade;
            player?.BackingPlayer.SaveSettingsFile((Int32)Synergy.Player.SettingsVersion.Zero);
        }
    }
}
