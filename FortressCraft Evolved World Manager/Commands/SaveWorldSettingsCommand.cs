﻿using System;
using System.Windows.Input;
using FortressCraft_Evolved_World_Manager.ViewModels;

namespace FortressCraft_Evolved_World_Manager.Commands
{
    public class SaveWorldSettingsCommand : ICommand
    {
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public Boolean CanExecute(Object parameter)
        {
            return true;
        }

        public void Execute(Object parameter)
        {
            WorldFacade world = parameter as WorldFacade;;
            world.BackingWorld.SaveSettings();
        }
    }
}
