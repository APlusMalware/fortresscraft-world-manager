﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using FortressCraft_Evolved_World_Manager.ViewModels;
using Synergy;
using Synergy.Installation;

namespace FortressCraft_Evolved_World_Manager.Commands
{
    public class ShowPlayerInventoryCommand : ICommand
    {
        private readonly Main _main;
        private Dictionary<Int32, ItemType> _itemTypes;
        private Dictionary<UInt16, CubeType> _cubeTypes;

        public ShowPlayerInventoryCommand(Main main)
        {
            _main = main;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public Boolean CanExecute(Object parameter)
        {
            return parameter is PlayerFacade;
        }

        public void Execute(Object parameter)
        {
            PlayerFacade player = parameter as PlayerFacade;
            if (player.BackingPlayer.Inventory == null)
            {
                _itemTypes = _main.GetItemTypes();
                _cubeTypes = _main.GetCubeTypes();
                player.BackingPlayer.LoadInventoryFile(_itemTypes);
            }

            List<PlayerInventoryItem> items = new List<PlayerInventoryItem>(player.BackingPlayer.Inventory.Items.Length);
            for (Int32 i = 0; i < Inventory.InventoryHeight; i++)
            {
                for (Int32 j = 0; j < Inventory.InventoryWidth; j++)
                {
                    Item item = player.BackingPlayer.Inventory.Items[j, i];
                    if (item == null)
                        items.Add(new PlayerInventoryItem(null));
                    else if (item.ItemCategory == ItemCategory.CubeStack)
                        items.Add(new PlayerInventoryItem(item) { CubeType = _cubeTypes[((ItemCubeStack) item).CubeTypeId]});
                    else
                        items.Add(new PlayerInventoryItem(item) { ItemType = _itemTypes[item.ItemTypeId]});
                }
            }
            _main.PlayerInventory = new PlayerInventory(items, _main.ShowChooseItemCommand);

            _main.ActiveView = Main.View.PlayerInventory;
        }
    }
}
