﻿using System;
using System.Windows.Input;
using FortressCraft_Evolved_World_Manager.ViewModels;

namespace FortressCraft_Evolved_World_Manager.Commands
{
    public class ShowWorldSettingsCommand : ICommand
    {
        private readonly Main _main;
        public ShowWorldSettingsCommand(Main main)
        {
            _main = main;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public Boolean CanExecute(Object parameter)
        {
            return parameter is WorldFacade;
        }

        public void Execute(Object parameter)
        {
            _main.WorldSettings = new WorldSettings(parameter as WorldFacade);
            _main.ActiveView = Main.View.WorldSettings;
        }
    }
}
