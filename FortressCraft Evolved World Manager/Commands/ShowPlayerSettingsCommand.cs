﻿using System;
using System.Windows.Input;
using FortressCraft_Evolved_World_Manager.ViewModels;

namespace FortressCraft_Evolved_World_Manager.Commands
{
    public class ShowPlayerSettingsCommand : ICommand
    {
        private readonly Main _main;
        public ShowPlayerSettingsCommand(Main main)
        {
            _main = main;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public Boolean CanExecute(Object parameter)
        {
            return parameter is PlayerFacade;
        }

        public void Execute(Object parameter)
        {
            PlayerFacade player = parameter as PlayerFacade;

            _main.PlayerSettings = new PlayerSettingsFacade(player.BackingPlayer.Settings);

            _main.ActiveView = Main.View.PlayerSettings;
        }
    }
}
