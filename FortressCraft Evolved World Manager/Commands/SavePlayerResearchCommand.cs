﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using FortressCraft_Evolved_World_Manager.ViewModels;

namespace FortressCraft_Evolved_World_Manager.Commands
{
    public class SavePlayerResearchCommand : ICommand
    {
        private readonly Main _main;

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public SavePlayerResearchCommand(Main main)
        {
            _main = main;
        }

        public Boolean CanExecute(Object parameter)
        {
            return parameter is PlayerFacade;
        }

        public void Execute(Object parameter)
        {
            PlayerFacade player = parameter as PlayerFacade;
            if (player == null)
                return;
            player.BackingPlayer.ProjectKeys = _main.PlayerResearch.AddedProjectKeys.ToList();
            player.BackingPlayer.RecipeKeys = _main.PlayerResearch.AddedRecipeKeys.ToList();
            player.BackingPlayer.SaveResearchFile((Int32)Synergy.Player.ResearchVersion.Zero);
        }
    }
}
