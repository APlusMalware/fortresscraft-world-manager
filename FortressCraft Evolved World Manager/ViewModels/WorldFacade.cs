﻿using System;
using Synergy;
using System.IO;
using Synergy.Terrain;

namespace FortressCraft_Evolved_World_Manager.ViewModels
{
    public class WorldFacade : NotifyPropertyChanged
    {
        private readonly World _backingWorld;

        public String FolderName { get { return new DirectoryInfo(_backingWorld.FullPath).Name; } }

        public String Name
        {
            get { return _backingWorld.Settings.Name; }
            set
            {
                _backingWorld.Settings.Name = value;
                OnPropertyChanged();
            }
        }

        public Int32 WorldSeed
        {
            get { return _backingWorld.Settings.WorldSeed; }
            set
            {
                _backingWorld.Settings.WorldSeed = value;
                OnPropertyChanged();
            }
        }

        public Single Gravity
        {
            get { return _backingWorld.Settings.Gravity; }
            set
            {
                _backingWorld.Settings.Gravity = value;
                OnPropertyChanged();
            }
        }

        public Single MovementSpeed
        {
            get { return _backingWorld.Settings.MovementSpeed; }
            set
            {
                _backingWorld.Settings.MovementSpeed = value;
                OnPropertyChanged();
            }
        }

        public Single JumpSpeed
        {
            get { return _backingWorld.Settings.JumpSpeed; }
            set
            {
                _backingWorld.Settings.JumpSpeed = value;
                OnPropertyChanged();
            }
        }

        public Single MaxFallingSpeed
        {
            get { return _backingWorld.Settings.MaxFallingSpeed; }
            set
            {
                _backingWorld.Settings.MaxFallingSpeed = value;
                OnPropertyChanged();
            }
        }

        public GameMode GameMode
        {
            get { return _backingWorld.Settings.GameMode; }
            set
            {
                _backingWorld.Settings.GameMode = value;
                OnPropertyChanged();
            }
        }

        public String InjectionSet
        {
            get { return _backingWorld.Settings.InjectionSet; }
            set
            {
                _backingWorld.Settings.InjectionSet = value;
                OnPropertyChanged();
            }
        }

        public Int32 ResourceLevel
        {
            get { return _backingWorld.Settings.ResourceLevel; }
            set
            {
                _backingWorld.Settings.ResourceLevel = value;
                OnPropertyChanged();
            }
        }

        public Int32 PowerLevel
        {
            get { return _backingWorld.Settings.PowerLevel; }
            set
            {
                _backingWorld.Settings.PowerLevel = value;
                OnPropertyChanged();
            }
        }

        public Int32 ConveyorLevel
        {
            get { return _backingWorld.Settings.ConveyorLevel; }
            set
            {
                _backingWorld.Settings.ConveyorLevel = value;
                OnPropertyChanged();
            }
        }

        public Int32 DayLevel
        {
            get { return _backingWorld.Settings.DayLevel; }
            set
            {
                _backingWorld.Settings.DayLevel = value;
                OnPropertyChanged();
            }
        }

        public DeathEffect DeathEffect
        {
            get { return _backingWorld.Settings.DeathEffect; }
            set
            {
                _backingWorld.Settings.DeathEffect = value;
                OnPropertyChanged();
            }
        }

        public Int64 CenteredSpawnX
        {
            get { return _backingWorld.Settings.SpawnLocation.X - CubeCoords.WorldCenter.X; }
            set
            {
                CubeCoords original = _backingWorld.Settings.SpawnLocation;
                _backingWorld.Settings.SpawnLocation = new CubeCoords(value + CubeCoords.WorldCenter.X, original.Y, original.Z);
                OnPropertyChanged();
            }
        }
        public Int64 CenteredSpawnY
        {
            get { return _backingWorld.Settings.SpawnLocation.Y - CubeCoords.WorldCenter.Y; }
            set
            {
                CubeCoords original = _backingWorld.Settings.SpawnLocation;
                _backingWorld.Settings.SpawnLocation = new CubeCoords(original.X, value + CubeCoords.WorldCenter.Y, original.Z);
                OnPropertyChanged();
            }
        }
        public Int64 CenteredSpawnZ
        {
            get { return _backingWorld.Settings.SpawnLocation.Z - CubeCoords.WorldCenter.Z; }
            set
            {
                CubeCoords original = _backingWorld.Settings.SpawnLocation;
                _backingWorld.Settings.SpawnLocation = new CubeCoords(original.X, original.Y, value + CubeCoords.WorldCenter.Z);
                OnPropertyChanged();
            }
        }

        public Int32 MobLevel
        {
            get { return _backingWorld.Settings.MobLevel; }
            set
            {
                _backingWorld.Settings.MobLevel = value;
                OnPropertyChanged();
            }
        }

        public Boolean IsIntroCompleted
        {
            get { return _backingWorld.Settings.IsIntroCompleted; }
            set
            {
                _backingWorld.Settings.IsIntroCompleted = value;
                OnPropertyChanged();
            }
        }

        public World BackingWorld { get { return _backingWorld; } }

        public WorldFacade(World backingWorld)
        {
            _backingWorld = backingWorld;
        }
    }
}
