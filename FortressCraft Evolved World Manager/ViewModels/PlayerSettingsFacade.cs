﻿using System;
using Synergy;
using System.IO;
using Synergy.Terrain;

namespace FortressCraft_Evolved_World_Manager.ViewModels
{
    public class PlayerSettingsFacade : NotifyPropertyChanged
    {
        private readonly PlayerSettings _backingPlayerSettings;

        public PlayerSettingsFacade(PlayerSettings playerSettings)
        {
            _backingPlayerSettings = playerSettings;
        }

        public Int32 CurrentWeaponType
        {
            get { return _backingPlayerSettings.CurrentWeaponType; }
            set
            {
                _backingPlayerSettings.CurrentWeaponType = value;
                OnPropertyChanged();
            }
        }
        
        public Int64 CenteredPositionX
        {
            get { return _backingPlayerSettings.WorldPosition.X - CubeCoords.WorldCenter.X; }
            set
            {
                CubeCoords original = _backingPlayerSettings.WorldPosition;
                _backingPlayerSettings.WorldPosition = new CubeCoords(value + CubeCoords.WorldCenter.X, original.Y, original.Z);
                OnPropertyChanged();
            }
        }
        public Int64 CenteredPositionY
        {
            get { return _backingPlayerSettings.WorldPosition.Y - CubeCoords.WorldCenter.Y; }
            set
            {
                CubeCoords original = _backingPlayerSettings.WorldPosition;
                _backingPlayerSettings.WorldPosition = new CubeCoords(original.X, value + CubeCoords.WorldCenter.Y, original.Z);
                OnPropertyChanged();
            }
        }
        public Int64 CenteredPositionZ
        {
            get { return _backingPlayerSettings.WorldPosition.Z - CubeCoords.WorldCenter.Z; }
            set
            {
                CubeCoords original = _backingPlayerSettings.WorldPosition;
                _backingPlayerSettings.WorldPosition = new CubeCoords(original.X, original.Y, value + CubeCoords.WorldCenter.Z);
                OnPropertyChanged();
            }
        }
    }
}
