﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using FortressCraft_Evolved_World_Manager.Commands;
using System;
using Synergy.Installation;

namespace FortressCraft_Evolved_World_Manager.ViewModels
{
    public class Main : NotifyPropertyChanged
    {
        private readonly Manager _manager;
        private ChooseWorld _chooseWorld;
        private WorldSettings _worldSettings;
        private PlayerList _playerList;
        private PlayerResearch _playerResearch;
        private PlayerSettingsFacade _playerSettings;
        private PlayerInventory _playerInventory;
        private ICommand _showWorldSettingsCommand;
        private ICommand _showChooseWorldCommand;
        private ICommand _saveWorldSettingsCommand;
        private ICommand _showPlayerListCommand;
        private ICommand _showPlayerResearchCommand;
        private ICommand _savePlayerResearchCommand;
        private ICommand _showPlayerSettingsCommand;
        private ICommand _savePlayerSettingsCommand;
        private ICommand _showPlayerInventoryCommand;
        private ICommand _showChooseItemCommand;
        private ICommand _savePlayerInventoryCommand;

        private View _activeView;

        public Main()
        {
            _manager = new Manager();
        }

        public enum View
        {
            ChooseWorld,
            WorldSettings,
            ChoosePlayer,
            PlayerResearch,
            PlayerSettings,
            PlayerInventory,
            ChooseItem
        }

        public View ActiveView
        {
            get { return _activeView; }
            set
            {
                _activeView = value;
                OnPropertyChanged();
            }
        }

        public ChooseWorld ChooseWorld
        {
            get
            {
                if (_chooseWorld == null)
                    _chooseWorld = new ChooseWorld(_manager, ShowWorldSettingsCommand);
                return _chooseWorld;
            }
            set
            {
                _chooseWorld = value;
                OnPropertyChanged();
            }
        }

        public WorldSettings WorldSettings
        {
            get
            {
                if (_worldSettings == null)
                    _worldSettings = new WorldSettings(ChooseWorld?.SelectedWorld);
                return _worldSettings;
            }
            set
            {
                _worldSettings = value;
                OnPropertyChanged();
            }
        }

        public PlayerList PlayerList
        {
            get
            {
                if (_playerList == null)
                    _playerList = new PlayerList(new PlayerFacade[0], null);
                return _playerList;
            }
            set
            {
                _playerList = value;
                OnPropertyChanged();
            }
        }

        public PlayerResearch PlayerResearch
        {
            get
            {
                if (_playerResearch == null)
                    _playerResearch = new PlayerResearch();
                return _playerResearch;
            }
            set
            {
                _playerResearch = value;
                OnPropertyChanged();
            }
        }

        public PlayerSettingsFacade PlayerSettings
        {
            get
            {
                if (_playerSettings == null)
                    _playerSettings = new PlayerSettingsFacade(new Synergy.PlayerSettings());
                return _playerSettings;
            }
            set
            {
                _playerSettings = value;
                OnPropertyChanged();
            }
        }

        public PlayerInventory PlayerInventory
        {
            get { return _playerInventory; }
            set
            {
                _playerInventory = value;
                OnPropertyChanged();
            }
        }

        public ICommand ShowWorldSettingsCommand
        {
            get
            {
                if (_showWorldSettingsCommand == null)
                    _showWorldSettingsCommand = new ShowWorldSettingsCommand(this);
                return _showWorldSettingsCommand;
            }
        }

        public ICommand ShowChooseWorldCommand
        {
            get
            {
                if (_showChooseWorldCommand == null)
                    _showChooseWorldCommand = new ShowChooseWorldCommand(this);
                return _showChooseWorldCommand;
            }
        }
        
        public ICommand SaveWorldSettingsCommand
        {
            get
            {
                if (_saveWorldSettingsCommand == null)
                    _saveWorldSettingsCommand = new SaveWorldSettingsCommand();
                return _saveWorldSettingsCommand;
            }
        }

        public ICommand ShowPlayerListCommand
        {
            get
            {
                if (_showPlayerListCommand == null)
                    _showPlayerListCommand = new ShowPlayerListCommand(this);
                return _showPlayerListCommand;
            }
        }

        public ICommand ShowPlayerResearchCommand
        {
            get
            {
                if (_showPlayerResearchCommand == null)
                    _showPlayerResearchCommand = new ShowPlayerResearchCommand(this);
                return _showPlayerResearchCommand;
            }
        }

        public ICommand SavePlayerResearchCommand
        {
            get
            {
                if (_savePlayerResearchCommand == null)
                    _savePlayerResearchCommand = new SavePlayerResearchCommand(this);
                return _savePlayerResearchCommand;
            }
        }

        public ICommand ShowPlayerSettingsCommand
        {
            get
            {
                if (_showPlayerSettingsCommand == null)
                    _showPlayerSettingsCommand = new ShowPlayerSettingsCommand(this);
                return _showPlayerSettingsCommand;
            }
        }

        public ICommand SavePlayerSettingsCommand
        {
            get
            {
                if (_savePlayerSettingsCommand == null)
                    _savePlayerSettingsCommand = new SavePlayerSettingsCommand();
                return _savePlayerSettingsCommand;
            }
        }

        public ICommand ShowPlayerInventoryCommand
        {
            get
            {
                if (_showPlayerInventoryCommand == null)
                    _showPlayerInventoryCommand = new ShowPlayerInventoryCommand(this);
                return _showPlayerInventoryCommand;
            }
        }

        public ICommand ShowChooseItemCommand
        {
            get
            {
                if (_showChooseItemCommand == null)
                    _showChooseItemCommand = new ShowChooseItemCommand(this);
                return _showChooseItemCommand;
            }
        }

        public ICommand SavePlayerInventoryCommand
        {
            get
            {
                if (_savePlayerInventoryCommand == null)
                    _savePlayerInventoryCommand = new SavePlayerInventoryCommand(this);
                return _savePlayerInventoryCommand;
            }
        }

        public void OpenWorld()
        {
            WorldSettings = new WorldSettings(ChooseWorld?.SelectedWorld);
        }

        public IEnumerable<String> GetProjectKeys()
        {
            return _manager.GetResearch().Select(kvp => kvp.Key);
        }

        public IEnumerable<String> GetRecipeKeys()
        {
            return _manager.GetRecipes().Select(kvp => kvp.Key);
        }

        public Dictionary<Int32, ItemType> GetItemTypes()
        {
            return _manager.GetItemTypes();
        }

        public Dictionary<UInt16, CubeType> GetCubeTypes()
        {
            return _manager.GetCubeTypes();
        } 
    }
}
