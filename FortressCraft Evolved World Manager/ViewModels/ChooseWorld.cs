﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Synergy;

namespace FortressCraft_Evolved_World_Manager.ViewModels
{
    public class ChooseWorld : NotifyPropertyChanged
    {
        private WorldFacade _selectedWorld;

        public ReadOnlyObservableCollection<WorldFacade> Worlds { get; }

        public ChooseWorld()
        {
        }

        public ChooseWorld(Manager manager, ICommand showWorldSettingsCommand)
        {
            List<World> worlds = World.ReadWorlds(manager.WorldsDir);
            var worldFacades = worlds.Select(w => new WorldFacade(w));
            Worlds = new ReadOnlyObservableCollection<WorldFacade>(new ObservableCollection<WorldFacade>(worldFacades));
            ShowWorldSettingsCommand = showWorldSettingsCommand;
        }

        public WorldFacade SelectedWorld
        {
            get { return _selectedWorld; }
            set
            {
                _selectedWorld = value;
                OnPropertyChanged();
            }
        }

        public ICommand ShowWorldSettingsCommand { get; }
    }
}
