﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace FortressCraft_Evolved_World_Manager.ViewModels
{
    public class PlayerInventory : NotifyPropertyChanged
    {
        private ObservableCollection<PlayerInventoryItem> _items;
        private PlayerInventoryItem _selectedItem;

        public PlayerInventory(IEnumerable<PlayerInventoryItem> items, ICommand showChooseItemCommand)
        {
            Items = new ObservableCollection<PlayerInventoryItem>(items);
            ShowChooseItemCommand = showChooseItemCommand;
        }

        public ObservableCollection<PlayerInventoryItem> Items
        {
            get { return _items; }
            set
            {
                _items = value;
                OnPropertyChanged();
            }
        }

        public PlayerInventoryItem SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                _selectedItem = value;
                OnPropertyChanged();
            }
        }

        public ICommand ShowChooseItemCommand { get; }
    }
}
