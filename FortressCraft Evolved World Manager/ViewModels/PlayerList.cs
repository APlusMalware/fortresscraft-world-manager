﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace FortressCraft_Evolved_World_Manager.ViewModels
{
    public class PlayerList : NotifyPropertyChanged
    {
        private readonly WorldFacade _world;
        private PlayerFacade _selectedPlayer;
        private ReadOnlyObservableCollection<PlayerFacade> _players;

        public WorldFacade World
        {
            get { return _world; }
        }

        public PlayerFacade SelectedPlayer
        {
            get { return _selectedPlayer; }
            set
            {
                _selectedPlayer = value;
                OnPropertyChanged();
            }
        }

        public ReadOnlyObservableCollection<PlayerFacade> Players
        {
            get { return _players; }
            set
            {
                _players = value;
                OnPropertyChanged();
            }
        }

        public PlayerList(IEnumerable<PlayerFacade> players, WorldFacade world)
        {
            _world = world;
            _players = new ReadOnlyObservableCollection<PlayerFacade>(new ObservableCollection<PlayerFacade>(players));
        }
    }
}
