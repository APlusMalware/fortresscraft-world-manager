﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Synergy;
using Synergy.Installation;


namespace FortressCraft_Evolved_World_Manager.ViewModels
{
    public class ChooseItem : NotifyPropertyChanged
    {
        private readonly IDictionary<Int32, ItemType> _itemTypeDictionary; 

        private ItemFacade _item;
        private ItemCategory _selectedItemCategory;

        public ItemFacade Item
        {
            get { return _item; }
            set
            {
                _item = value;
                OnPropertyChanged();
            }
        }

        public ReadOnlyObservableCollection<ItemCategory> CategoryTypes { get; }
        public ReadOnlyObservableCollection<ItemType> ItemTypes { get; }
        public ReadOnlyObservableCollection<CubeType> CubeTypes { get; }

        public ItemCategory SelectedItemCategory
        {
            get { return _selectedItemCategory; }
            set
            {
                _selectedItemCategory = value;
                OnPropertyChanged();
            }
        }
        public ItemType SelectedItemType
        {
            get { return _itemTypeDictionary[_item.TypeId]; }
            set
            {
                _item.ItemCategory = value.ItemCategory;
                _item.TypeId = value.ItemId;
                OnPropertyChanged();
            }
        }
        public CubeType SelectedCubeType
        {
            get { return _item.CubeType; }
            set
            {
                _item.CubeTypeId = value.TypeId;
                OnPropertyChanged();
            }
        }

        public Boolean IsSelectedItemCategoryCubeStack { get { return _selectedItemCategory == ItemCategory.CubeStack; } }
        public Boolean IsSelectedItemCategorySingle { get { return _selectedItemCategory != ItemCategory.CubeStack; } }

        public Boolean IsItemCategoryStack { get { return _item.ItemCategory == ItemCategory.Stack; } }
        public Boolean IsItemCategoryCubeStack { get { return _item.ItemCategory == ItemCategory.CubeStack; } }
        public Boolean IsItemCategoryDurability { get { return _item.ItemCategory == ItemCategory.Durability; } }
        public Boolean IsItemCategoryCharge { get { return _item.ItemCategory == ItemCategory.Charge; } }
        public Boolean IsItemCategoryLocation { get { return _item.ItemCategory == ItemCategory.Location; } }

        public Item SelectedItem
        {
            get { return Item.BackingItem; }
            set
            {
                Item.BackingItem = value;
                OnPropertyChanged();
            }
        }
        
        public ChooseItem(IDictionary<Int32, ItemType> itemTypes, IEnumerable<CubeType> cubeTypes, Item item)
        {
            _itemTypeDictionary = itemTypes;
            _selectedItemCategory = item?.ItemCategory != ItemCategory.CubeStack ? ItemCategory.Single : ItemCategory.CubeStack;
            CategoryTypes = new ReadOnlyObservableCollection<ItemCategory>(new ObservableCollection<ItemCategory>(new List<ItemCategory> {ItemCategory.Single, ItemCategory.CubeStack}));
            ItemTypes = new ReadOnlyObservableCollection<ItemType>(new ObservableCollection<ItemType>(itemTypes.Values));
            CubeTypes = new ReadOnlyObservableCollection<CubeType>(new ObservableCollection<CubeType>(cubeTypes));
            Item = new ItemFacade();
            SelectedItem = item;
        }
    }
}