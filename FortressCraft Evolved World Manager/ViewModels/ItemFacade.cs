﻿using System;
using System.Numerics;
using Synergy;
using Synergy.Installation;
using Synergy.Terrain;

namespace FortressCraft_Evolved_World_Manager.ViewModels
{
    public class ItemFacade : NotifyPropertyChanged
    {
        private ItemCategory _itemCategory;
        private ItemType _itemType;
        private Int32 _typeId;

        private Int32 _stackAmount;

        private CubeType _cubeType;
        private UInt16 _cubeTypeId;
        private UInt16 _cubeData;
        private Int32 _cubeStackAmount;

        private Single _currentDurability;
        private Single _maxDurability;

        private Single _charge;

        private CubeCoords _location;
        private Vector3 _look;
        
        public ItemCategory ItemCategory
        {
            get { return _itemCategory; }
            set
            {
                _itemCategory = value;
                OnPropertyChanged();
            }
        }

        public ItemType ItemType
        {
            get { return _itemType; }
            set
            {
                _itemType = value;
                OnPropertyChanged();
            }
        }
        public Int32 TypeId
        {
            get { return _typeId; }
            set
            {
                _typeId = value;
                OnPropertyChanged();
            }
        }

        public Int32 StackAmount
        {
            get { return _stackAmount; }
            set
            {
                _stackAmount = value;
                OnPropertyChanged();
            }
        }

        public CubeType CubeType
        {
            get { return _cubeType; }
            set
            {
                _cubeType = value;
                OnPropertyChanged();
            }
        }
        public UInt16 CubeTypeId
        {
            get { return _cubeTypeId; }
            set
            {
                _cubeTypeId = value;
                OnPropertyChanged();
            }
        }
        public UInt16 CubeData
        {
            get { return _cubeData; }
            set
            {
                _cubeData = value;
                OnPropertyChanged();
            }
        }
        public Int32 CubeStackAmount
        {
            get { return _cubeStackAmount; }
            set
            {
                _cubeStackAmount = value;
                OnPropertyChanged();
            }
        }

        public Single CurrentDurability
        {
            get { return _currentDurability; }
            set
            {
                _currentDurability = value;
                OnPropertyChanged();
            }
        }
        public Single MaxDurability
        {
            get { return _maxDurability; }
            set
            {
                _maxDurability = value;
                OnPropertyChanged();
            }
        }

        public Single Charge
        {
            get { return _charge; }
            set
            {
                _charge = value;
                OnPropertyChanged();
            }
        }

        public Int64 LocationX
        {
            get { return _location.X; }
            set
            {
                _location = new CubeCoords (value, _location.Y, _location.Z);
                OnPropertyChanged();
            }
        }
        public Int64 LocationY
        {
            get { return _location.Y; }
            set
            {
                _location = new CubeCoords(_location.X, value, _location.Z);
                OnPropertyChanged();
            }
        }
        public Int64 LocationZ
        {
            get { return _location.Z; }
            set
            {
                _location = new CubeCoords(_location.X, _location.Y, value);
                OnPropertyChanged();
            }
        }

        public Single LookX
        {
            get { return _look.X; }
            set
            {
                _look = new Vector3(value, _look.Y, _look.Z);
                OnPropertyChanged();
            }
        }
        public Single LookY
        {
            get { return _look.Y; }
            set
            {
                _look = new Vector3(_look.X, value, _look.Z);
                OnPropertyChanged();
            }
        }
        public Single LookZ
        {
            get { return _look.Z; }
            set
            {
                _look = new Vector3(_look.X, _look.Y, value);
                OnPropertyChanged();
            }
        }

        public Item BackingItem
        {
            get
            {
                Item item = Item.GetNewItem(TypeId, ItemCategory);
                switch (item.ItemCategory)
                {
                    case ItemCategory.Stack:
                        ItemStack stack = item as ItemStack;
                        stack.Amount = StackAmount;
                        break;
                    case ItemCategory.CubeStack:
                        ItemCubeStack cubeStack = item as ItemCubeStack;
                        cubeStack.CubeTypeId = CubeTypeId;
                        cubeStack.Amount = CubeStackAmount;
                        cubeStack.CubeData = CubeData;
                        break;
                    case ItemCategory.Durability:
                        ItemDurability durability = item as ItemDurability;
                        durability.CurrentDurability = CurrentDurability;
                        durability.MaxDurablitity = MaxDurability;
                        break;
                    case ItemCategory.Charge:
                        ItemCharge charge = item as ItemCharge;
                        charge.Charge = Charge;
                        break;
                    case ItemCategory.Location:
                        ItemLocation location = item as ItemLocation;
                        location.Location = new CubeCoords(LocationX, LocationY, LocationZ);
                        location.Look = new Vector3(LookX, LookY, LookZ);
                        break;
                }
                return item;
            }
            set
            {
                Item item = value ?? new Item();
                TypeId = item.ItemTypeId;
                ItemCategory = item.ItemCategory;
                switch (item.ItemCategory)
                {
                    case ItemCategory.Stack:
                        ItemStack stack = item as ItemStack;
                        StackAmount = stack.Amount;
                        break;
                    case ItemCategory.CubeStack:
                        ItemCubeStack cubeStack = item as ItemCubeStack;
                        CubeTypeId = cubeStack.CubeTypeId;
                        CubeStackAmount = cubeStack.Amount;
                        CubeData = cubeStack.CubeData;
                        break;
                    case ItemCategory.Durability:
                        ItemDurability durability = item as ItemDurability;
                        CurrentDurability = durability.CurrentDurability;
                        MaxDurability = durability.MaxDurablitity;
                        break;
                    case ItemCategory.Charge:
                        ItemCharge charge = item as ItemCharge;
                        Charge = charge.Charge;
                        break;
                    case ItemCategory.Location:
                        ItemLocation location = item as ItemLocation;
                        LocationX = location.Location.X;
                        LocationY = location.Location.Y;
                        LocationZ = location.Location.Z;
                        LookX = location.Look.X;
                        LookY = location.Look.Y;
                        LookZ = location.Look.Z;
                        break;
                }
                OnPropertyChanged();
            }
        }
    }
}
