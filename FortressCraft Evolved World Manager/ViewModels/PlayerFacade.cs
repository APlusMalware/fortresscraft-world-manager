﻿using Synergy;
using System;

namespace FortressCraft_Evolved_World_Manager.ViewModels
{
    public class PlayerFacade : NotifyPropertyChanged
    {
        private readonly Player _backingPlayer;

        public String Id
        {
            get { return _backingPlayer.Id; }
        }

        public Int32 ResearchPoints
        {
            get { return _backingPlayer.ResearchPoints; }
            set
            {
                _backingPlayer.ResearchPoints = value;
                OnPropertyChanged();
            }
        }

        public Player BackingPlayer { get { return _backingPlayer; } }

        public PlayerFacade(Player backingPlayer)
        {
            _backingPlayer = backingPlayer;
        }

        public override String ToString()
        {
            return _backingPlayer.ToString();
        }
    }
}
