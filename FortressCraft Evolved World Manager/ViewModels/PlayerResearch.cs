﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using FortressCraft_Evolved_World_Manager.Commands;

namespace FortressCraft_Evolved_World_Manager.ViewModels
{
    public class PlayerResearch : NotifyPropertyChanged
    {
        private ObservableCollection<String> _addedProjectKeys;
        private ObservableCollection<String> _addedRecipeKeys;
        private ObservableCollection<String> _unaddedProjectKeys;
        private ObservableCollection<String> _unaddedRecipeKeys;
        private String _selectedAddProjectKey;
        private String _selectedAddRecipeKey;
        private String _selectedUnaddedProjectKey;
        private String _selectedUnaddedRecipeKey;
        private ICommand _addProjectKeyCommand;
        private ICommand _addRecipeKeyCommand;
        private ICommand _removeProjectKeyCommand;
        private ICommand _removeRecipeKeyCommand;
        private readonly PlayerFacade _player;

        public PlayerResearch()
        {
        }

        public PlayerResearch(PlayerFacade player, IEnumerable<String> allProjectKeys, IEnumerable<String> allRecipeKeys)
        {
            _player = player;

            if (player.BackingPlayer.RecipeKeys == null || player.BackingPlayer.ProjectKeys == null)
                player.BackingPlayer.LoadReasearchFile();

            var addedProjectKeys = player.BackingPlayer.ProjectKeys;
            var unaddedProjectKeys = allProjectKeys.Except(addedProjectKeys);

            var addedRecipeKeys = player.BackingPlayer.RecipeKeys;
            var unaddedRecipeKeys = allRecipeKeys.Except(addedRecipeKeys);

            AddedProjectKeys = new ObservableCollection<String>(addedProjectKeys);
            UnaddedProjectKeys = new ObservableCollection<String>(unaddedProjectKeys);
            AddedRecipeKeys = new ObservableCollection<String>(addedRecipeKeys);
            UnaddedRecipeKeys = new ObservableCollection<String>(unaddedRecipeKeys);
        }

        public PlayerFacade Player
        {
            get { return _player; }
        }

        public ObservableCollection<String> AddedProjectKeys
        {
            get { return _addedProjectKeys; }
            set
            {
                _addedProjectKeys = value;
                _player.BackingPlayer.ProjectKeys = value.ToList();
                OnPropertyChanged();
            }
        }

        public ObservableCollection<String> AddedRecipeKeys
        {
            get { return _addedRecipeKeys; }
            set
            {
                _addedRecipeKeys = value;
                _player.BackingPlayer.RecipeKeys = value.ToList();
                OnPropertyChanged();
            }
        }

        public ObservableCollection<String> UnaddedProjectKeys
        {
            get { return _unaddedProjectKeys; }
            set
            {
                _unaddedProjectKeys = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<String> UnaddedRecipeKeys
        {
            get { return _unaddedRecipeKeys; }
            set
            {
                _unaddedRecipeKeys = value;
                OnPropertyChanged();
            }
        }

        public String SelectedAddProjectKey
        {
            get { return _selectedAddProjectKey; }
            set
            {
                _selectedAddProjectKey = value;
                OnPropertyChanged();
            }
        }

        public String SelectedAddRecipeKey
        {
            get { return _selectedAddRecipeKey; }
            set
            {
                _selectedAddRecipeKey = value;
                OnPropertyChanged();
            }
        }

        public String SelectedUnaddedProjectKey
        {
            get { return _selectedUnaddedProjectKey; }
            set
            {
                _selectedUnaddedProjectKey = value;
                OnPropertyChanged();
            }
        }

        public String SelectedUnaddedRecipeKey
        {
            get { return _selectedUnaddedRecipeKey; }
            set
            {
                _selectedUnaddedRecipeKey = value;
                OnPropertyChanged();
            }
        }

        public ICommand AddProjectKeyCommand
        {
            get
            {
                if (_addProjectKeyCommand == null)
                    _addProjectKeyCommand = new MoveKeyCommand(AddedProjectKeys, UnaddedProjectKeys);
                return _addProjectKeyCommand;
            }
        }

        public ICommand AddRecipeKeyCommand
        {
            get
            {
                if (_addRecipeKeyCommand == null)
                    _addRecipeKeyCommand = new MoveKeyCommand(AddedRecipeKeys, UnaddedRecipeKeys);
                return _addRecipeKeyCommand;
            }
        }

        public ICommand RemoveProjectKeyCommand
        {
            get
            {
                if (_removeProjectKeyCommand == null)
                    _removeProjectKeyCommand = new MoveKeyCommand(UnaddedProjectKeys, AddedProjectKeys);
                return _removeProjectKeyCommand;
            }
        }

        public ICommand RemoveRecipeKeyCommand
        {
            get
            {
                if (_removeRecipeKeyCommand == null)
                    _removeRecipeKeyCommand = new MoveKeyCommand(UnaddedRecipeKeys, AddedRecipeKeys);
                return _removeRecipeKeyCommand;
            }
        }
    }
}
