﻿using Synergy;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FortressCraft_Evolved_World_Manager.ViewModels
{
    public class WorldSettings : NotifyPropertyChanged
    {
        private WorldFacade _selectedWorld;

        public WorldFacade SelectedWorld
        {
            get { return _selectedWorld; }
            set
            {
                _selectedWorld = value;
                OnPropertyChanged();
            }
        }

        public IEnumerable<GameMode> GameModes
        {
            get { return Enum.GetValues(typeof(GameMode)).Cast<GameMode>(); }
        }

        public IEnumerable<DeathEffect> DeathEffects
        {
            get { return Enum.GetValues(typeof(DeathEffect)).Cast<DeathEffect>(); }
        }

        public WorldSettings(WorldFacade world)
        {
            _selectedWorld = world;
        }
    }
}
