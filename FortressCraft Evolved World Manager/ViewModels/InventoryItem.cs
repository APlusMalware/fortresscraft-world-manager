﻿using System;
using System.Linq;
using Synergy;
using Synergy.Installation;

namespace FortressCraft_Evolved_World_Manager.ViewModels
{
    public class PlayerInventoryItem : NotifyPropertyChanged
    {
        private ItemType _itemType;

        private CubeType _cubeType;

        private Item _backingItem;

        public PlayerInventoryItem(Item item)
        {
            _backingItem = item;
        }

        public ItemType ItemType
        {
            get { return _itemType; }
            set
            {
                _itemType = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(Id));
                OnPropertyChanged(nameof(ItemCategory));
                OnPropertyChanged(nameof(Category));
                OnPropertyChanged(nameof(Name));
            }
        }

        public CubeType CubeType
        {
            get { return _cubeType; }
            set
            {
                _cubeType = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(Id));
                OnPropertyChanged(nameof(ItemCategory));
                OnPropertyChanged(nameof(Category));
                OnPropertyChanged(nameof(Name));
            }
        }

        public String Name
        {
            get
            {
                if (_itemType != null)
                {
                    return _itemType.Name;
                }
                else if (_cubeType != null)
                {
                    return _cubeType.Values.FirstOrDefault(c => c.Value == ((ItemCubeStack)_backingItem).CubeData)?.Name ?? _cubeType.Name;
                }
                else
                {
                    return String.Empty;
                }
            }
        }

        public String Category
        {
            get
            {
                if (_itemType != null)
                {
                    return _itemType.Category;
                }
                else if (_cubeType != null)
                {
                    return _cubeType.Category;
                }
                else
                {
                    return String.Empty;
                }
            }
        }

        public ItemCategory? ItemCategory
        {
            get { return _itemType?.ItemCategory ?? (_cubeType != null ? (ItemCategory?)Synergy.Installation.ItemCategory.CubeStack : null); }
        }

        public Int32? Id
        {
            get { return _itemType?.ItemId ?? _cubeType?.TypeId; }
        }

        public Int32? StackSize
        {
            get
            {
                if (_backingItem?.ItemCategory == Synergy.Installation.ItemCategory.CubeStack)
                    return ((ItemCubeStack) _backingItem).Amount;
                else if (_backingItem?.ItemCategory == Synergy.Installation.ItemCategory.Stack)
                    return ((ItemStack) _backingItem).Amount;
                else
                    return null;
            }
        }

        public Item BackingItem
        {
            get { return _backingItem; }
            set
            {
                _backingItem = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(StackSize));
            }
        }
    }
}
