﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using Synergy.Installation;

namespace FortressCraft_Evolved_World_Manager.Converters
{
    public class ItemCategoryToVisibilityConverter : IValueConverter
    {
        public ItemCategory TriggerValue { get; set; }
        public Boolean VisibileIfSet { get; set; }
        public Boolean IsCollapsed { get; set; }

        public Object Convert(Object value, Type targetType, Object parameter, CultureInfo cultureInfo)
        {
            if (!(value is ItemCategory))
                return DependencyProperty.UnsetValue;
            ItemCategory category = (ItemCategory) value;

            if (category == TriggerValue)
            {
                if (VisibileIfSet)
                    return Visibility.Visible;
                else
                    return IsCollapsed ? Visibility.Collapsed : Visibility.Hidden;
            }
            else
            {
                if (!VisibileIfSet)
                    return Visibility.Visible;
                else
                    return IsCollapsed ? Visibility.Collapsed : Visibility.Hidden;
            }
        }

        public Object ConvertBack(Object value, Type targetType, Object parameter, CultureInfo cultureInfo)
        {
            return DependencyProperty.UnsetValue;
        }
    }
}
