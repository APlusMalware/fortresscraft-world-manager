﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace FortressCraft_Evolved_World_Manager.Converters
{
    public class BooleanToVisibilityConverter : IValueConverter
    {
        public Object Convert(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            Boolean visible = value is Boolean ? (Boolean)value : false;
            return visible ? Visibility.Visible : Visibility.Hidden;
        }

        public Object ConvertBack(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            Visibility visibility = value is Visibility ? (Visibility)value : Visibility.Hidden;
            return visibility == Visibility.Visible;
        }
    }
}
