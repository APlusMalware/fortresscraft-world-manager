﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using static FortressCraft_Evolved_World_Manager.ViewModels.Main;

namespace FortressCraft_Evolved_World_Manager.Converters
{
    public class MainViewToVisibilityConverter : IValueConverter
    {
        public View TriggerValue { get; set; }
        public Boolean VisibileIfSet { get; set; }
        public Boolean IsCollapsed { get; set; }

        public Object Convert(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            if (!(value is View))
                return DependencyProperty.UnsetValue;

            View view = (View)value;

            if (view == TriggerValue)
            {
                if (VisibileIfSet)
                    return Visibility.Visible;
                else
                    return IsCollapsed ? Visibility.Collapsed : Visibility.Hidden;
            }
            else
            {
                if (!VisibileIfSet)
                    return Visibility.Visible;
                else
                    return IsCollapsed ? Visibility.Collapsed : Visibility.Hidden;
            }
        }

        public Object ConvertBack(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
