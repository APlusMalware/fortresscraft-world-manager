﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using Synergy.Installation;

namespace FortressCraft_Evolved_World_Manager.Converters
{
    // This name seems a little verbose... But it's accurate.
    public class ItemTypeItemCategoryToVisibilityConverter : IValueConverter
    {
        public ItemCategory TriggerValue { get; set; }
        public Boolean VisibileIfSet { get; set; }
        public Boolean IsCollapsed { get; set; }

        public Object Convert(Object value, Type targetType, Object parameter, CultureInfo cultureInfo)
        {
            if (!(value is ItemType))
                return Visibility.Collapsed;
            ItemCategory category = ((ItemType)value).ItemCategory;

            if (category == TriggerValue)
            {
                if (VisibileIfSet)
                    return Visibility.Visible;
                else
                    return IsCollapsed ? Visibility.Collapsed : Visibility.Hidden;
            }
            else
            {
                if (!VisibileIfSet)
                    return Visibility.Visible;
                else
                    return IsCollapsed ? Visibility.Collapsed : Visibility.Hidden;
            }
        }

        public Object ConvertBack(Object value, Type targetType, Object parameter, CultureInfo cultureInfo)
        {
            return DependencyProperty.UnsetValue;
        }
    }
}
